from django.core.management.base import BaseCommand
from catalog.models import Category, Product
from django.db.models import Count


class Command(BaseCommand):
    def handle(self, *args, **options):

        print(Product.objects.filter(pk=5).delete_real())
