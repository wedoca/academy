from django.core.management.base import BaseCommand
from catalog.models import Category, Product
from django.db.models import Count


class Command(BaseCommand):
    def handle(self, *args, **options):

        p = Category.objects\
            .filter(product__price__gte=100)\
            .annotate(num=Count('product'))\
            .values('id', 'name', 'num')\
            .filter(num__gt=2)

        for i in p:
            print(i)

        print('-' * 40)

        product_list = Product.objects.select_related('category').values('name', 'category__name', 'price')
        for p in product_list:
            print(p['category__name'], p['name'], p['price'])
