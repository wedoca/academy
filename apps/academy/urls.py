from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^candidate/$', views.CandidateCreateView.as_view(), name='candidate-new'),

    url(r'^test-list/$', views.TestTestListView.as_view(), name='test-list'),

    url(r'^test/(?P<slug>[a-z0-9_-]+)/$', views.test_detail_view, name='test-detail'),

    url(r'^jedi/$', views.JediListView.as_view(), name='jedi-list'),

    url(r'^candidate/list/(?P<jedi_pk>\d+)/$', views.candidate_list, name='candidate-list'),

    url(r'^candidate/add/$', views.candidate_add, name='candidate-add'),
]
