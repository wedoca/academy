from django.contrib import admin
from .models import Planet, Jedi, Candidate, TestTest, Question, Choice, TestResult, TestResultAnswer


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline]
    list_display = ('question_text',)


class TestResultAnswerInline(admin.TabularInline):
    model = TestResultAnswer
    extra = 0
    readonly_fields = ['question', 'choice']


class TestResultAdmin(admin.ModelAdmin):
    inlines = [TestResultAnswerInline]


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'planet', 'mentor')


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)
admin.site.register(Planet)
admin.site.register(Jedi)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(TestTest)
admin.site.register(TestResult, TestResultAdmin)
