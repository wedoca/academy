from django.db import models
from django.urls import reverse


class Planet(models.Model):
    """ Планета """
    name = models.CharField('Наименование', max_length=100, unique=True)

    class Meta:
        verbose_name = 'Планета'
        verbose_name_plural = 'Планеты'

    def __str__(self):
        return self.name


class Jedi(models.Model):
    """ Джедай """
    name = models.CharField('Имя', max_length=20, unique=True)
    planet = models.ForeignKey(Planet)

    class Meta:
        verbose_name = 'Джедай'
        verbose_name_plural = 'Джедаи'
        ordering = ['-id']

    def __str__(self):
        return '{} / {}'.format(self.name, self.planet)

    @property
    def padavan_count(self):
        """ Количество подаванов в обучении """
        return Candidate.objects.filter(mentor=self).count()


class Candidate(models.Model):
    """ Кандидат """
    name = models.CharField('Имя', max_length=20)
    planet = models.ForeignKey(Planet)
    age = models.PositiveSmallIntegerField('Возраст',  help_text='Укажите сколько вам полных лет.')
    email = models.EmailField('Email', max_length=30, unique=True)
    mentor = models.ForeignKey(Jedi, null=True, blank=True)

    class Meta:
        verbose_name = 'Кандидат'
        verbose_name_plural = 'Кандидаты'
        ordering = ['-id']

    def __str__(self):
        return '{} / {} / {}'.format(self.name, self.planet, self.email)


class TestTest(models.Model):
    """ Тестовое испытание """
    name = models.CharField('Название испытания', max_length=100)
    slug = models.SlugField('Код ордена', unique=True, max_length=100)

    class Meta:
        verbose_name = 'Тестовое испытание'
        verbose_name_plural = 'Тестовые испытания'
        ordering = ['-id']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('academy:test-detail', args=[self.slug])


class Question(models.Model):
    """ Вопрос """
    test_test = models.ForeignKey(TestTest)
    question_text = models.CharField('Вопрос', max_length=200)

    def __str__(self):
        return self.question_text

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class Choice(models.Model):
    """ Вариант ответа """
    question = models.ForeignKey(Question)
    choice_text = models.CharField('Вариант ответа', max_length=200)
    votes = models.IntegerField('Количество баллов', default=0)

    def __str__(self):
        return self.choice_text

    class Meta:
        verbose_name = 'Вариант ответа'
        verbose_name_plural = 'Варианты ответа'


class TestResult(models.Model):
    """ результат испытания """
    candidate = models.ForeignKey(Candidate)
    test = models.ForeignKey(TestTest)
    complete = models.BooleanField('Задание пройдено', default=False)
    points = models.PositiveSmallIntegerField('Количество баллов')

    def __str__(self):
        return '{} / {}'.format(self.candidate, self.test)

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'
        ordering = ['-id']


class TestResultAnswer(models.Model):
    """ Ответ на вопрос испытания """
    test = models.ForeignKey(TestResult)
    question = models.ForeignKey(Question)
    choice = models.ForeignKey(Choice)

    def __str__(self):
        return '{} / {}'.format(self.question, self.choice)

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
