from django.views.generic import CreateView, TemplateView
from .models import TestTest, Question, TestResult, TestResultAnswer, Choice, Jedi, Planet, Candidate
from .forms import CandidateForms
from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.db.models import Count


class JediListView(TemplateView):
    """ Страница выбора джедая """
    model = Jedi
    template_name = 'pages/jedi-list.html'

    def get_context_data(self, **kwargs):
        context = super(JediListView, self).get_context_data(**kwargs)

        # вывод всех джедаев
        context['jedi_all'] = Jedi.objects.all()

        # вывод джедаев у которых есть хотя бы один падаван
        context['jedi_count'] = Jedi.objects.annotate(c_num=Count('candidate')).filter(c_num__gt=0)

        return context


class CandidateCreateView(CreateView):
    """ Страница с формой для кандидата """
    success_url = reverse_lazy('academy:test-detail', args=['first'])
    template_name = 'pages/candidate-new.html'
    form_class = CandidateForms

    def form_valid(self, form):
        self.object = form.save()

        # записываем в сессию id кандидата
        self.request.session['candidate_id'] = self.object.id

        return super(CandidateCreateView, self).form_valid(form)


class TestTestListView(generic.ListView):
    """ Список испытаний """
    template_name = 'pages/test-list.html'
    context_object_name = 'items'

    def get_queryset(self):
        return TestTest.objects.all()[:5]


def test_detail_view(request, slug):
    """ Просмотр и прохождение испытания """
    template_name = 'pages/test-detail.html'

    # получаем испытание
    test = get_object_or_404(TestTest, slug=slug)
    question_form = list()

    candidate_id = request.session.get('candidate_id')
    if not candidate_id:
        messages.warning(request, 'Сначала необходимо зарегистрироваться как кандидат')
        return redirect('home_page')

    if request.method == 'POST':
        for k, v in request.POST.items():
            if k.startswith('question'):
                question_form.append(v)

        # Если количество ответов меньше чем вопросов
        if Question.objects.filter(test_test_id=test.id).count() > len(question_form):
            messages.error(request, 'Что то пошло не так. Попробуйте еще раз.')
            messages.error(request, 'Количество ответов меньше чем вопросов.')
            return redirect('academy:test-detail', test.slug)

        result = TestResult.objects.create(
            candidate_id=candidate_id,
            test=test,
            points=0
        )

        choice_list = {str(c.id): c for c in Choice.objects.filter(question__test_test_id=test.id)}
        for q in question_form:
            choice = choice_list.get(q)

            if choice is None:
                messages.error(request, 'Что то пошло не так. Попробуйте еще раз. choice не найтен')
                return redirect('academy:test-detail', test.slug)

            TestResultAnswer.objects.create(
                test=result,
                question=choice.question,
                choice=choice
            )
            result.points += choice.votes

        result.save()

        request.session.pop('candidate_id')
        messages.success(request, 'Задание пройдено и будет проверено в ближайшее время')
        return redirect('home_page')

    return render(
        request,
        template_name,
        {'item': test}
    )


def candidate_list(request, jedi_pk):
    """ Список кандидатов отфильтрованных по планете """
    template_name = 'pages/candidate-list.html'

    jedi = get_object_or_404(Jedi, pk=jedi_pk)

    candidates = Candidate.objects.filter(mentor__isnull=True, planet=jedi.planet)
    return render(
        request,
        template_name,
        {
            'candidates': candidates,
            'jedi': jedi.id
        }
    )


def candidate_add(request):
    """ Добавление кандита к себе на обучение """
    if request.method == 'POST':
        jedi_id = request.POST.get('jedi')
        candidate_id = request.POST.get('candidate')
        jedi = get_object_or_404(Jedi, pk=int(jedi_id))
        candidate = get_object_or_404(Candidate, pk=int(candidate_id))

        padavan_count = Candidate.objects.filter(mentor=jedi).count()
        if padavan_count >= 3:
            messages.error(request, 'У джедая не может быть более трех учеников')
            return redirect('academy:candidate-list', jedi.id)

        candidate.mentor = jedi
        candidate.save()

        messages.success(request, 'Кандидат был зачислен в падаваны')
        return redirect('academy:candidate-list', jedi.id)

    messages.error(request, 'Что то пошло не так')
    return redirect('academy:jedi-list')
