from django import forms
from .models import Candidate
from django.contrib.auth import get_user_model
user = get_user_model()


class CandidateForms(forms.ModelForm):
    """ Форма для регистрации кандидата """

    age = forms.IntegerField(
        label='Укажите ваш возраст',
        max_value=99,
        min_value=18
    )

    class Meta:
        model = Candidate
        fields = ('name', 'planet', 'age', 'email')
