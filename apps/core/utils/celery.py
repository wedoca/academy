from celery import shared_task
from django.core.mail import send_mail
from django.conf import settings


@shared_task
def task_send_mail(subject=None, text_plain='', default_from_email=settings.DEFAULT_FROM_EMAIL, email=None, html_message=None):
    """ async send message on the email """
    if not email or not subject:
        return

    send = send_mail(
        subject,
        text_plain,
        default_from_email,
        [email],
        html_message=html_message,
        fail_silently=True
    )
    return send