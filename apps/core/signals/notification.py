from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from academy.models import Candidate
from django.conf import settings


@receiver(post_save, sender=Candidate)
def after_create_or_change_user(sender, instance, created, **kwargs):
    """ Изменение кандидата """
    if instance.mentor:
        ''' Если у кандидата есть учитель - если кандидат стал падаваном '''

        email = instance.email
        subject = 'Зачисление в падаваны {}'.format(instance.mentor.name)
        text_plain = ''
        default_from_email = settings.DEFAULT_FROM_EMAIL
        html_message = '<h1>Поздравляем</h1><p>Джедай {} взял вас в ученики.</p>'.format(instance.mentor.name)

        # отправляем ему сообщение
        send = send_mail(
            subject,
            text_plain,
            default_from_email,
            [email, ],
            html_message=html_message,
            fail_silently=True
        )
        return send
