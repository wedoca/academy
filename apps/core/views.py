from django.views.generic import TemplateView


class HomePage(TemplateView):
    """ Представление для главной страницы """
    template_name = 'home.html'
