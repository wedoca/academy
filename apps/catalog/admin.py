from django.contrib import admin
from .models import Category, Product, Phones, Items


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'category', 'price', 'active')
    # list_editable = ('price', )

class ItemsAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'status')


class PhonesAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone', 'users')


admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(Phones, PhonesAdmin)
admin.site.register(Items, ItemsAdmin)
