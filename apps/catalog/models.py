from django.db import models


class ProductQuerySet(models.QuerySet):

    def delete(self):
        self.update(active=True)

    def delete_real(self):
        super(ProductQuerySet, self).delete()


class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)


class Category(models.Model):
    name = models.CharField('Группа товара', max_length=64)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name='Группа')
    name = models.CharField('Название товара', max_length=128)
    price = models.DecimalField('Стоимость единицы, руб.', max_digits=10, decimal_places=2)
    active = models.BooleanField('active', default=True)

    objects = ProductManager()

    def __str__(self):
        return '{} / {}'.format(self.name, self.price)


class Phones(models.Model):
    phone = models.CharField('phone', max_length=10)
    users = models.IntegerField('users')


class Items(models.Model):
    status_list = (
        (3, 'не продан'),
        (7, 'продан'),
        (5, 'резерв')
    )
    user_id = models.IntegerField('user id')
    status = models.SmallIntegerField(choices=status_list)
