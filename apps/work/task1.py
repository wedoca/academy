import re, unittest


def foo(s):
    p = re.compile(r'\([^)]+$')
    return re.sub(p, '', s)


class MyTest(unittest.TestCase):

    def test_base_example(self):
        string = 'esdfd((esdf)(esdf'
        result = 'esdfd((esdf)'
        self.assertEqual(foo(string), result)

    def test_expample2(self):
        string = 'esdfd((esdf)(esdf(a)(b)(abdf(adfd(aaa'
        result = 'esdfd((esdf)(esdf(a)(b)'
        self.assertEqual(foo(string), result)

if __name__ == '__main__':
    unittest.main()